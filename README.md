# Elvarg - OSRS PK

---

**This was released on [Rune-Server](https://www.rune-server.ee/runescape-development/rs2-server/advertise/652701-osrs-pk-spawn-instant-pk-osrs-bounty-hunter-amazing-combat.html) by [Professor Oak](https://www.rune-server.ee/members/professor+oak/).**

---

>  I've decided to release OSRS Pk, which is Elvarg based. I can simply say that I've lost most of my motivation in RSPS, even though I'll still keep it online for people to play. My previous Elvarg release became somewhat popular and I kinda feel bad, because it wasn't worth all the good attention it was getting. There were plenty of things done poorly but hopefully this will make up for that.